%define commit 708a11da2c8f998c03512fa0dda6d9f2005345a2
%define commit_short 708a11d
Name:           eww
Version:        0.5.0.tray_3
Release:        %autorelease
Summary:        ElKowars wacky widgets

License:        MIT
URL:            https://github.com/elkowar/eww/
Source0:        %{url}/archive/%{commit_short}.tar.gz

BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(gtk-layer-shell-0)
BuildRequires: pkgconfig(gobject-2.0)
BuildRequires: pkgconfig(dbusmenu-gtk3-0.4)
BuildRequires: cargo

Provides:      eww
Provides:      eww-wayland

%description
Elkowars Wacky Widgets is a standalone widget system made in Rust that allows
you to implement your own, custom widgets in any window manager.

%prep
%autosetup -n eww-%{commit}

%build
cargo build --release --package eww --no-default-features --features wayland

%install
install -Dm755 target/release/eww -t %{buildroot}%{_bindir}

%files
%license LICENSE
%doc examples/ README.md
%{_bindir}/eww


%changelog
%autochangelog
