# rpmbuild -bs SPECS/eww.spec && mock --enable-network SRPMS/eww-0.5.0.tray_3-1.fc39.src.rpm

default:
    just --list

build-image:
    podman build --tag rpm-builder ./

# Enter and run, eg `enter-and-run "helix-git" "bash"`
enter-and-run package commands:
    podman run --rm  --volume /home/flkz/copr_packages/fedora/{{ package }}/:/home/mockbuilder/rpmbuild/SPECS/ --privileged -ti rpm-builder sh -c "{{ commands }}"

# Run `rpmlint`
lint package:
    just enter-and-run {{ package }} "rpmlint SPECS/{{ package }}.spec"

# Run `spectool -g -R` to download sources, `rpmbuild  -bs` to generate src.rpm, and `mock` to test installation.
mock package:
    just enter-and-run {{ package }} "\
    	spectool -g -R SPECS/{{ package }}.spec && \
    	rpmbuild -bs SPECS/{{ package }}.spec && \
    	mock --enable-network SRPMS/{{ package }}*.src.rpm"
